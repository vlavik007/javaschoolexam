package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

import static java.util.Objects.isNull;

public class Subsequence {

    private boolean validateDecision;

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (badConditions(x, y)) {
            throw new IllegalArgumentException("Bad conditions");
        } else if (validateInputs(x, y)) {
            return validateDecision;
        } else {
            return calculate(x, y);
        }
    }

    private boolean badConditions(List goal, List source) {
        return isNull(goal) || isNull(source);
    }

    private boolean validateInputs(List goal, List source) {
        if (goal.isEmpty()) {
            validateDecision = true;
            return true;
        } else if (goal.size() > source.size()) {
            validateDecision = false;
            return true;
        }

        return false;
    }

    private boolean calculate(List goalSeq, List sourceSeq) {
        int processedGoalElementsNumber = 0;
        int startingElemOfCurrentIteration = 0;

        while (true) {

            for (int i = startingElemOfCurrentIteration; i < sourceSeq.size(); ++i) {

                if (sourceSeq.get(i).equals(goalSeq.get(processedGoalElementsNumber))) {

                    startingElemOfCurrentIteration = sourceSeq.indexOf(
                            goalSeq.get(processedGoalElementsNumber++)) + 1;

                    if (goalSeq.size() == processedGoalElementsNumber) {
                        return true;
                    } else {
                        break;
                    }
                }

                if (haveReachedTheEndOfSequence(goalSeq, processedGoalElementsNumber)) {
                    return true;
                }

                if (isThereEnoughRemainingElementsToCheckCondition(sourceSeq, processedGoalElementsNumber, i)) {
                    return false;
                }
            }
        }
    }

    private boolean haveReachedTheEndOfSequence(List goalSeq, int processedGoalElementsNumber) {
        return goalSeq.size() + 1 == processedGoalElementsNumber;
    }

    private boolean isThereEnoughRemainingElementsToCheckCondition(
            List sourceSeq, int processedGoalElementsNumber, int i) {
        return i + 1 > sourceSeq.size() - processedGoalElementsNumber - 1;
    }
}