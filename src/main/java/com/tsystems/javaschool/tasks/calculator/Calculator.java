package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class Calculator {

    private List<String> operatorList;
    private List<String> operandList;

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(final String statement) {

        if (isExpressionIncorrect(statement)) {
            return null;
        }

        final Double result = calculate(statement);

        if (null == result) {
            return null;
        }

        return isIntegerNumber(result)
                ? String.valueOf(result.intValue())
                : String.valueOf(result);
    }

    private boolean isExpressionIncorrect(final String statement) {

        if (Objects.isNull(statement) || statement.isEmpty()) {
            return true;
        }

        parseStatementToOperatorsAndOperands(statement);
        if (operandsLessThanOperators()) {
            return true;
        }

        if (!checkBalancedParenthesis(statement)) {
            for (String operand : operandList) {
                if (!isPureNumber(operand)) return true;
            }
        } else {
            return true;
        }

        return false;
    }

    private boolean isIntegerNumber(final Double result) {
        return (result % 1) == 0;
    }

    private Double calculate(final String statement) {
        return new Object() {
            int position = -1;
            int currentChar;

            Double parse() {
                nextChar();
                Double x = parseExpression();

                if (position < statement.length()) {
                    throw new RuntimeException("Illegal character: " + (char) currentChar);
                }

                if (Double.isInfinite(x)) {
                    return null;
                }

                return x;
            }

            void nextChar() {
                currentChar = (++position < statement.length())
                        ? statement.charAt(position)
                        : -1;
            }

            boolean readyToTake(int charToTake) {

                while (' ' == currentChar) {
                    nextChar();
                }

                if (charToTake == currentChar) {
                    nextChar();
                    return true;
                }

                return false;
            }

            Double parseExpression() {
                Double x = parseTerm();

                while (true) {

                    if (readyToTake('+')) {
                        x += parseTerm();
                    } else if (readyToTake('-')) {
                        x -= parseTerm();
                    } else {
                        return x;
                    }
                }
            }

            Double parseTerm() {
                Double x = parseFactor();

                while (true) {

                    if (readyToTake('*')) {
                        x *= parseFactor();
                    } else if (readyToTake('/')) {
                        x /= parseFactor();
                    } else {
                        return x;
                    }
                }
            }

            Double parseFactor() {

                if (readyToTake('+')) {
                    return parseFactor();
                }
                if (readyToTake('-')) {
                    return -parseFactor();
                }

                Double x;
                int startPosition = this.position;

                if (readyToTake('(')) {
                    x = parseExpression();
                    readyToTake(')');
                } else if ((currentChar >= '0' && currentChar <= '9') || currentChar == '.') {
                    while ((currentChar >= '0' && currentChar <= '9') || currentChar == '.') {
                        nextChar();
                    }
                    x = Double.parseDouble(statement.substring(startPosition, this.position));
                } else {
                    throw new RuntimeException("Illegal character: " + (char) currentChar);
                }

                return x;
            }
        }.parse();
    }

    private void parseStatementToOperatorsAndOperands(final String statement) {

        operatorList = new ArrayList<>();
        operandList = new ArrayList<>();

        StringTokenizer st = new StringTokenizer(statement, "+-*/", true);

        while (st.hasMoreTokens()) {
            String token = st.nextToken();

            if ("+-/*".contains(token)) {
                operatorList.add(token);
            } else {
                operandList.add(token);
            }
        }
    }

    private boolean operandsLessThanOperators() {
        return operandList.size() <= operatorList.size();
    }

    private boolean isPureNumber(String value) {
        if (!isBlankString(value = value.replaceAll("[\\[\\](){}]", ""))) {
            return value.matches("^(\\d+\\.?)+$");
        }
        return false;
    }

    private boolean checkBalancedParenthesis(final String expression) {

        if (expression.isEmpty()) {
            return false;
        }

        Stack<Character> stack = new Stack<>();

        for (int i = 0; i < expression.length(); i++) {
            char current = expression.charAt(i);

            if (current == '{' || current == '(' || current == '[') {
                stack.push(current);
            }
            if (current == '}' || current == ')' || current == ']') {
                if (stack.isEmpty()) {
                    return false;
                }
                char last = stack.peek();
                if (current == '}' && last == '{' || current == ')'
                        && last == '(' || current == ']' && last == '[') {
                    stack.pop();
                } else {
                    return false;
                }
            }
        }

        return !stack.isEmpty();
    }

    private boolean isBlankString(String value) {
        return (value == null || value.equals("") || value.equals("null") || value.trim().equals(""));
    }
}
