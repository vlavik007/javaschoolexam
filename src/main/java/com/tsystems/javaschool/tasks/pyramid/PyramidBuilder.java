package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        if (((0 == inputNumbers.get(0)) && (0 == inputNumbers.get(1)))
                || inputNumbers.contains(null) || inputNumbers.isEmpty()
                || isSumOfArithmeticProgression(inputNumbers.size())) {
            throw new CannotBuildPyramidException();
        }

        List<Integer> sortedList = new ArrayList<>(inputNumbers);
        Collections.sort(sortedList);

        int numberOfRows = calculateNumberOfRows(sortedList);

        return buildMatrix(numberOfRows, sortedList);
    }

    private boolean isSumOfArithmeticProgression(int size) {
        int sum;
        int n = 1;

        do {
            sum = n * (n + 1) / 2;
            if (sum == size) {
                return false;
            }
            ++n;
        } while (sum < size);

        return true;
    }

    private int calculateNumberOfRows(List<Integer> array) {
        int rows = 0;
        int numberOfCountedIntegers = 0;

        while (numberOfCountedIntegers < array.size()) {
            ++rows;
            for (int cols = 0; cols < rows; ++cols) {
                ++numberOfCountedIntegers;
            }
        }

        return rows;
    }

    private int[][] buildMatrix(int numberOfRows, List<Integer> array) {

        int cols = numberOfRows + (numberOfRows - 1);

        int[][] matrix = new int[numberOfRows][cols];

        int currentIndex = array.size() - 1;

        int[] currentPosition = getRowPosition(numberOfRows);

        for (int i = numberOfRows - 1; i >= 0; --i) {
            for (int j = cols - 1; j >= 0; --j) {
                if (currentPosition[j] == 1) {
                    matrix[i][j] = array.get(currentIndex--);
                } else {
                    matrix[i][j] = 0;
                }
            }
            currentPosition = getRowPosition(currentPosition);
        }

        return matrix;
    }

    private int[] getRowPosition(int numberOfElements) {
        int cols = numberOfElements + (numberOfElements - 1);
        int[] position = new int[cols];
        Arrays.fill(position, 0);
        for (int i = 0; i < cols; ++i) {
            if (i % 2 == 0) {
                position[i] = 1;
            }
        }

        return position;
    }

    private int[] getRowPosition(int[] position) {

        int indexWithNoZero = findIndexWithNoZero(position);

        for (int i = 0; i < position.length; ++i) {

            if (i >= indexWithNoZero) {
                if (i == indexWithNoZero) {
                    position[i] = 0;
                } else {
                    position[i] = (position[i - 1] == 0) ? 1 : 0;
                }
            }

            if (i + 1 == position.length - indexWithNoZero) {
                position[i] = 0;
                break;
            }
        }

        return position;
    }

    private int findIndexWithNoZero(int[] position) {

        for (int i = 0; i < position.length; ++i) {
            if (position[i] == 1) {
                return i;
            }
        }
        return 0;
    }
}
